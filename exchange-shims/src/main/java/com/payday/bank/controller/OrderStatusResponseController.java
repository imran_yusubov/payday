package com.payday.bank.controller;

import com.payday.bank.client.StocksClient;
import com.payday.bank.common.dto.OrderStatusDto;
import java.util.Map;
import java.util.Optional;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("orders")
@RequiredArgsConstructor
public class OrderStatusResponseController {

    private final StocksClient stocksClient;

    private Map<String, String> orderStatusMapping = Map.of(
            "new", "submitted",
            "filled", "filled",
            "failed", "failed",
            "cancelled", "cancelled"
    );


    @PostMapping //@ToDo: should use something , to make it idempotent.& need to implement auth token
    public void acceptResponseStatus(@RequestBody @Valid OrderStatusDto statusDto) {
        log.trace("Status received:" + statusDto);
        //@ToDo: need to extract it into  service and handle status mapping dynamically
        //
        final String orderStatus = Optional.of(statusDto.getStatus()).map((status) ->
                Optional.of(orderStatusMapping.get(status.toLowerCase()))
                        .orElse("failed")
        ).orElse("failed");

        stocksClient.updateOrderStatus(statusDto, orderStatus);
    }

}
