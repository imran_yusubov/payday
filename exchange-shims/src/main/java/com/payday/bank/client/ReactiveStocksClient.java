package com.payday.bank.client;

import com.payday.bank.common.dto.StockDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;


@Slf4j
@Service
//@ToDo: this class does a lot, need to split
public class ReactiveStocksClient {

    private final KafkaTemplate<String, StockDto> stocksTemplate;
    private final WebClient client;
    private final String stocksTopic;

    public ReactiveStocksClient(KafkaTemplate<String, StockDto> stocksTemplate,
            @Value("${exchange.stocks_api_live}") String stockApi,
            @Value("${kafka.topics.stocks.live}") String stocksTopic) {
        client = WebClient.builder().baseUrl(stockApi).build();
        this.stocksTemplate = stocksTemplate;
        this.stocksTopic = stocksTopic;
    }

    public void receiveStockPrices() {
        Flux<StockDto> stocks = client.get().retrieve().bodyToFlux(StockDto.class);
        stocks.subscribe(this::publishToTopic);
    }

    private void publishToTopic(StockDto stockDto) {
        stocksTemplate.send(stocksTopic, stockDto);
    }

}
