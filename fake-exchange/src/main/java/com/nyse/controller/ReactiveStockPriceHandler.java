package com.nyse.controller;

import com.nyse.dto.StockDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.kafka.receiver.KafkaReceiver;
import reactor.kafka.receiver.ReceiverRecord;

@Slf4j
@RestController
public class ReactiveStockPriceHandler {

    @Value(value = "${kafka.bootstrapAddress}")
    private String bootstrapAddress;

    @Autowired
    KafkaReceiver<String, StockDto> receiver;

    @GetMapping(value = "/stocks", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<StockDto> getLivePrices() {
        System.out.println("Receiving messages");
        Flux<ReceiverRecord<String, StockDto>> kafkaFlux = receiver.receive();

        return kafkaFlux.log().doOnNext(r -> {
            r.receiverOffset().acknowledge();
            System.out.println("Sending....");
        })
                .map(ReceiverRecord::value);
    }


}
