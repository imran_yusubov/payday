package com.nyse.schedule;

import com.nyse.dto.StockDto;
import com.nyse.model.orders.Order;
import com.nyse.model.orders.OrderStatus;
import com.nyse.model.stocks.Stock;
import com.nyse.repository.OrderRepository;
import com.nyse.repository.StocksRepository;
import com.nyse.service.TransitionService;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service
public class VolatilityScheduler {

    private final StocksRepository stocksRepository;
    private final OrderRepository orderRepository;
    private final TransitionService transitionService;
    private final KafkaTemplate<String, StockDto> stockDtoKafkaTemplate;
    private final ModelMapper modelMapper;

    @Scheduled(fixedRate = 10000)
    public void createVolatility() {
        long start = System.currentTimeMillis();
        log.info("Updating indexes");
        stocksRepository.findAll().forEach((s) -> calculatePrice(s));
        log.info("Updating indexes completed in {}", System.currentTimeMillis() - start);
    }

    private void calculatePrice(Stock stock) {
        final double random = Math.random();
        double volatility = 0.01;
        double changePercent = 2 * volatility * random;
        if (changePercent > volatility) {
            changePercent -= (2 * volatility);
        }
        double changeAmount = stock.getCurrent() * changePercent;
        double newPrice = stock.getCurrent() + changeAmount;
        save(stock, newPrice);
    }

    private void save(Stock stock, Double newPrice) {
        // log.info("symbol: {} current: {} new: {}", stock.getSymbol(), stock.getCurrent(), newPrice);
        stock.setCurrent(newPrice);
        stocksRepository.save(stock);
        updateOrders(stock);
    }

    private void updateOrders(Stock stock) {
        //Update buy orders
        List<Order> buy = orderRepository
                .findByStockIdAndStatusAndMaxPriceGreaterThanAndType(stock.getStockId(), OrderStatus.NEW,
                        stock.getCurrent(), "BUY");
        updateOrders(buy, stock);

        //Update sell orders
        final List<Order> sell = orderRepository
                .findByStockIdAndStatusAndMaxPriceLessThanAndType(stock.getStockId(), OrderStatus.NEW,
                        stock.getCurrent(), "SELL");
        updateOrders(sell, stock);
        final StockDto stockDto = modelMapper.map(stock, StockDto.class);
        stockDtoKafkaTemplate.send("stocks", stockDto);
    }

    private void updateOrders(List<Order> orders, Stock stock) {
        orders.stream()
                .forEach(order -> {
                    log.trace("Updating order {}", order);
                    order.setFilledPrice(stock.getCurrent());
                    orderRepository.save(order);
                    transitionService.transitionOrder(order.getId(), "filled");
                });
    }

}
