package com.payday.bank.common.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import lombok.Data;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.index.Indexed;

@Entity
@Table(name = Holding.TABLE_NAME, indexes = {
        //  @Index(columnList = Holding.STOCK_COLUMN, name = Holding.STOCK_INDEX, unique = true),
        @Index(columnList = Holding.ACCOUNT_COLUMN, name = Holding.ACCOUNT_INDEX, unique = true)
})
@Data
@RedisHash(Holding.TABLE_NAME)
public class Holding {

    public static final String TABLE_NAME = "HOLDINGS";
    public static final String INDEX = "_index";
    public static final String STOCK_COLUMN = "stock_id";
    public static final String ACCOUNT_COLUMN = "account_id";
    public static final String STOCK_INDEX = STOCK_COLUMN + INDEX;
    public static final String ACCOUNT_INDEX = ACCOUNT_COLUMN + INDEX;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Indexed
    private String stockId;

    // @JoinColumn(name = ACCOUNT_COLUMN)
    @Indexed
    @Column(name = ACCOUNT_COLUMN)
    private Long accountId;

    private Long numberOfShares;
    private Double current;
    private Double averagePrice;
}
