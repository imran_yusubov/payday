package com.payday.bank.common.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.index.Indexed;

@Entity
@Data
@Table(name = "pstock")
@RedisHash(value = "pstock")
public class PStock {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Double open;
    private Double close;
    private Double current;
    private String stockId;
    private String index;
    private Double max;
    private Double min;
    @Indexed
    private boolean preferential;
}
