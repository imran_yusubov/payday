package com.payday.bank.common.dto;

import lombok.Data;

@Data
public class OrderStatusDto {

    private Long id;
    private Long externalId;
    private String status;
    private Double filledPrice;
}
