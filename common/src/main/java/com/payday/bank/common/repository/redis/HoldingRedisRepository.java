package com.payday.bank.common.repository.redis;

import com.payday.bank.common.model.Holding;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HoldingRedisRepository extends CrudRepository<Holding, Long> {

    List<Holding> findByStockId(String stockId);
    List<Holding> findByAccountId(Long accountId);
}
