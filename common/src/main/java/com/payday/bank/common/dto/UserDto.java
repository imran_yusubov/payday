package com.payday.bank.common.dto;

import com.payday.bank.common.model.user.UserRole;
import java.util.Date;
import lombok.Data;

@Data
public class UserDto {

    private long id;

    private String userName;
    private String password;
    private String email;
    private UserRole role;

    private Date created;
}
