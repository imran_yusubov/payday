package com.payday.bank.services;

import com.payday.bank.common.model.Account;
import com.payday.bank.common.model.Holding;
import com.payday.bank.common.repository.redis.AccountRedisRepository;
import com.payday.bank.common.repository.redis.HoldingRedisRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class AccountBalanceService {

    private final HoldingRedisRepository holdingRedisRepository;
    private final AccountRedisRepository accountRedisRepository;

    //@ToDo: better to use shedlock and kafka queue to ensure each account is processed only once in multipod env
    @Async
    @Scheduled(fixedRate = 10000)
    public void recalculateTotalBalance() {
        log.trace("Running schedule");
        accountRedisRepository.findAll().forEach(this::recalculateTotalBalance);
    }

    private void recalculateTotalBalance(Account account) {
        log.trace("Recalculating account balance {}",account);
        Double stocksTotalBalance = 0.0;
        for (Holding h : holdingRedisRepository.findByAccountId(account.getId())) {
            log.trace("Found holdings {} for account {}",h,account );
            stocksTotalBalance = stocksTotalBalance + h.getNumberOfShares() * h.getCurrent();
        }
        account.setTotalBalance(account.getCashBalance() + stocksTotalBalance);
        accountRedisRepository.save(account);
    }
}
