package com.payday.bank;

import com.payday.bank.common.config.LoggingTcpConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import({LoggingTcpConfiguration.class})
public class NotificationsManagerApplication {


    public static void main(String[] args) {
        SpringApplication.run(NotificationsManagerApplication.class, args);
    }


}
