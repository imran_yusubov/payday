package com.payday.bank.service;

import com.payday.bank.common.dto.AccountDto;
import com.payday.bank.common.dto.CreateOrderDto;
import com.payday.bank.common.dto.GenericSearchDto;
import com.payday.bank.common.model.Account;
import com.payday.bank.common.model.Holding;
import com.payday.bank.common.repository.redis.AccountRedisRepository;
import com.payday.bank.common.repository.search.SearchCriteria;
import com.payday.bank.common.repository.search.SearchOperation;
import com.payday.bank.common.repository.search.SearchSpecification;
import com.payday.bank.dto.OrderDto;
import com.payday.bank.exceptions.InvalidStateException;
import com.payday.bank.exceptions.NotFoundException;
import com.payday.bank.model.orders.Order;
import com.payday.bank.model.orders.OrderStatus;
import com.payday.bank.repository.AccountRepository;
import com.payday.bank.repository.HoldingRepository;
import com.payday.bank.repository.OrderRepository;
import com.payday.bank.transitions.Pending;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@RequiredArgsConstructor
public class OrderService {

    private final ModelMapper mapper;
    private final AccountService accountService;
    private final AccountRepository accountRepository;
    private final OrderRepository orderRepository;
    private final TransitionService transitionService;
    private final AccountRedisRepository accountRedisRepository;
    private final HoldingRepository holdingRepository;

    /**
     * Creates a buy sell order and places it in stock exchange. Validates if the account is holding enough shares on
     * sell orders & enough cash on buy orders.
     *
     * @param createOrderDto : the order details should be places in the exchange
     * @param userName : the user name of the account holder
     * @return : the created dto details
     */
    @Transactional
    public OrderDto placeOrder(CreateOrderDto createOrderDto, String userName) {
        final AccountDto accountDto = accountService.findAccountByUserName(userName);
        final Optional<Account> accountOptional = accountRepository.findById(accountDto.getId());
        if (accountOptional.isPresent()) {
            final Order order = placeOrder(createOrderDto, accountOptional.get());
            final OrderDto orderDto = mapper.map(order, OrderDto.class);
            transitionService.transitionOrder(orderDto, Pending.NAME);
            return orderDto;
        } else {
            throw new NotFoundException("Account not found dd");
        }
    }

    /**
     * Returns all orders for given user account
     */
    @Transactional
    public Page<OrderDto> findAllOrders(GenericSearchDto filter, Pageable pageable, String user) {
        final AccountDto accountByUserName = accountService.findAccountByUserName(user);
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setKey("account");
        searchCriteria.setValue(accountByUserName.getId());
        searchCriteria.setOperation(SearchOperation.IN);
        filter.addCriteria(searchCriteria);
        return orderRepository.findAll(new SearchSpecification<>(filter.getCriteria()), pageable)
                .map(p -> mapper.map(p, OrderDto.class));
    }

    /**
     * Retrieves an account by id
     *
     * @param id : the id of the account
     * @return : the account details
     */
    @Transactional
    public Optional<OrderDto> findOrderById(Long id) {
        return orderRepository.findById(id)
                .map(o -> mapper.map(o, OrderDto.class));
    }

    private Order placeOrder(CreateOrderDto createOrderDto, Account account) {
        if (createOrderDto.getType().equalsIgnoreCase("BUY")) {
            return placeBuyOrder(createOrderDto, account);
        } else if (createOrderDto.getType().equalsIgnoreCase("SELL")) {
            return placeSellOrder(createOrderDto, account);
        } else {
            throw new InvalidStateException(
                    "Bad operation type, should be BUY/SELL, but was:" + createOrderDto.getType());
        }
    }

    private Order placeSellOrder(CreateOrderDto createOrderDto, Account account) {
        if (hasEnoughShares(createOrderDto, account)) {
            Order order = mapOrder(createOrderDto, account);
            return orderRepository.save(order);
        } else {
            throw new InvalidStateException(
                    "The account is holding less shares than " + createOrderDto.getNumberOfShares() + " of the stock "
                            + createOrderDto.getStockId());
        }
    }

    private Order placeBuyOrder(CreateOrderDto createOrderDto, Account account) {
        if (hasEnoughCash(createOrderDto, account)) {
            Order order = mapOrder(createOrderDto, account);
            account.setCashBalance(account.getCashBalance() - minimumRequiredAmountToBuy(createOrderDto));
            accountRedisRepository.save(account);
            return order;
        } else {
            throw new InvalidStateException(
                    "Not enough cash in balance, required at least " + minimumRequiredAmountToBuy(createOrderDto)
                            + " available "
                            + account.getCashBalance());
        }
    }

    private Order mapOrder(CreateOrderDto createOrderDto, Account account) {
        Order order = mapper.map(createOrderDto, Order.class);
        order.setStatus(OrderStatus.NEW);
        order.setAccount(account);
        orderRepository.save(order);
        return order;
    }

    private boolean hasEnoughShares(CreateOrderDto createOrderDto, Account account) {
        final Holding holding = holdingRepository
                .findByAccountIdAndStockId(account.getId(), createOrderDto.getStockId())
                .orElseThrow(() -> new InvalidStateException(
                        "The account is not holding any shares of the stock " + createOrderDto.getStockId()));
        return holding.getNumberOfShares() >= createOrderDto.getNumberOfShares();
    }

    private boolean hasEnoughCash(CreateOrderDto createOrderDto, Account account) {
        return account.getCashBalance() > minimumRequiredAmountToBuy(createOrderDto);
    }

    private Double minimumRequiredAmountToBuy(CreateOrderDto createOrderDto) {
        return createOrderDto.getMaxPrice() * createOrderDto.getNumberOfShares();
    }

}
