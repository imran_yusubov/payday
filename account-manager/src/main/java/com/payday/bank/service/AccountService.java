package com.payday.bank.service;

import com.payday.bank.common.dto.AccountDto;
import com.payday.bank.common.dto.Email;
import com.payday.bank.common.dto.HoldingDto;
import com.payday.bank.common.dto.UserDto;
import com.payday.bank.common.model.Account;
import com.payday.bank.common.model.Holding;
import com.payday.bank.common.model.user.User;
import com.payday.bank.common.repository.redis.AccountRedisRepository;
import com.payday.bank.common.repository.redis.HoldingRedisRepository;
import com.payday.bank.dto.OrderDto;
import com.payday.bank.exceptions.AlreadyExistException;
import com.payday.bank.exceptions.NotFoundException;
import com.payday.bank.notifications.EmailNotificationService;
import com.payday.bank.repository.AccountRepository;
import com.payday.bank.repository.HoldingRepository;
import com.payday.bank.repository.UserRepository;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@RequiredArgsConstructor
public class AccountService {

    private final UserRepository userRepository;
    private final AccountRepository accountRepository;
    private final ModelMapper mapper;
    private final EmailNotificationService emailNotificationService;
    private final HoldingRepository holdingRepository;
    private final AccountRedisRepository accountRedisRepository;
    private final HoldingRedisRepository holdingRedisRepository;

    /**
     * Creates a new account for given user
     *
     * @param userName : user uid
     */
    @Transactional
    public AccountDto createAccount(String userName) {
        log.trace("Creating new account for user {}", userName);
        final User user = findUser(userName);
        Account account = Account
                .builder()
                .cashBalance(0.0)
                .totalBalance(0.0)
                .user(user)
                .build();
        if (findByUser(user).isPresent()) {
            throw new AlreadyExistException("An account for the user is already exist: " + userName);
        }
        account.setUser(findUser(userName));
        final Account save = accountRepository.save(account);
        accountRedisRepository.save(save);
        sendEmailNotification(userName, account.getUser().getEmail());
        return mapper.map(account, AccountDto.class);
    }

    /**
     * Retrieves the account of the given user
     *
     * @param id: account uid
     * @return : the account dto
     */
    @Transactional
    public AccountDto findAccountById(Long id) {
        log.trace("Find account by id: {} ", id);
        final Account account = accountRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Account not found with id " + id));
        final AccountDto accountDto = mapper.map(account, AccountDto.class);
        accountDto.setUser(mapper.map(account.getUser(), UserDto.class));
        return accountDto;
    }

    /**
     * Retrieves the account of the given user
     *
     * @param userName: user uid
     * @return : the account dto
     */
    @Transactional
    public AccountDto findAccountByUserName(String userName) {
        return findByUser(userName)
                .map((account) -> {
                    AccountDto accountDto = mapper.map(account, AccountDto.class);
                    accountRedisRepository.findById(account.getId()).ifPresent((a) ->
                            accountDto.setTotalBalance(a.getTotalBalance())
                    );
                    accountDto.setUser(mapper.map(account.getUser(), UserDto.class));
                    final Set<HoldingDto> collect = account.getStocks().stream()
                            .map((s) -> mapper.map(s, HoldingDto.class))
                            .collect(Collectors.toSet());
                    accountDto.setStocks(collect);
                    return accountDto;
                }).orElseThrow(() -> accountNotFount(userName));
    }

    /**
     * Deposits cash to given user account
     *
     * @param amount : the amount to be deposited, positive
     * @param userName : the account holder user name
     */
    @Transactional
    //@CacheEvict(value = "account-single", key = "#account.id")
    public AccountDto deposit(Long amount, String userName) {
        if (amount < 0) {
            throw new IllegalArgumentException("The amount must be greater than 0");
        }
        final Account account = findByUser(userName).orElseThrow(() -> accountNotFount(userName));
        account.setCashBalance(account.getCashBalance() + amount);
        accountRedisRepository.save(account);
        return mapper.map(account, AccountDto.class);
    }

    /**
     * Update account details
     *
     * @param orderDto order details
     */
    @Transactional
    public void applyOrder(OrderDto orderDto) {
        final Account account = accountRepository.findById(orderDto.getAccount().getId())
                .orElseThrow(() -> new RuntimeException("Account not found with id " + orderDto.getAccount().getId()));
        if (orderDto.getType().equalsIgnoreCase("buy")) {
            addStocks(orderDto, account);
            updateAccountHoldings(account, orderDto);
        } else if (orderDto.getType().equalsIgnoreCase("sell")) {
            account.setCashBalance(account.getCashBalance() + orderDto.getMaxPrice() * orderDto.getNumberOfShares());
            updateAccountHoldings(account, orderDto);
        }
    }

    private void updateAccountHoldings(Account account, OrderDto orderDto) {
        log.trace("Updating account holdings");
        holdingRepository.findByAccountIdAndStockId(account.getId(), orderDto.getStockId())
                .ifPresent((h) -> {
                    log.trace("Account holding {}", h);
                    long shares;
                    if (orderDto.getType().equalsIgnoreCase("sell")) {
                        shares = h.getNumberOfShares() - orderDto.getNumberOfShares();
                    }
                    shares = h.getNumberOfShares() ;

                    if (shares > 0) {
                        h.setNumberOfShares(shares);
                        final Holding save = holdingRepository.save(h);
                        holdingRedisRepository.save(save);
                    } else {
                        //@ToDd: rework this part to make it declerative
                        holdingRepository.delete(h);
                        holdingRedisRepository.delete(h);
                    }
                });
    }

    private void addStocks(OrderDto orderDto, Account account) {
        Optional<Holding> holdingOptional = holdingRepository
                .findByAccountIdAndStockId(account.getId(), orderDto.getStockId());
        Holding holding = null;
        if (holdingOptional.isPresent()) {
            log.trace("Holding {} is present already", orderDto.getStockId());
            holding = holdingOptional.get();
            double average = getAveragePrice(holding, orderDto);
            holding.setAveragePrice(average);
            holding.setAveragePrice(average);
            holding.setAccountId(account.getId());
            holding.setNumberOfShares(getTotalShares(holding, orderDto));
            holding.setStockId(orderDto.getStockId());
            //@ToDo: release cash back to account if the fill prices is less than max price
            account.setCashBalance(account.getCashBalance() + calculateAmountToRelease(orderDto));

        } else {
            log.trace("Holding {} is not present", orderDto.getStockId());
            holding = new Holding();
            holding.setAveragePrice(orderDto.getFilledPrice());
            holding.setCurrent(orderDto.getFilledPrice());
            holding.setNumberOfShares(orderDto.getNumberOfShares());
            holding.setAccountId(account.getId());
            holding.setStockId(orderDto.getStockId());
            account.setCashBalance(account.getCashBalance() + calculateAmountToRelease(orderDto));
        }
        //@ToDd: rework this part to make it declerative
        holdingRepository.save(holding);
        holdingRedisRepository.save(holding);
    }

    private Double getAveragePrice(Holding holding, OrderDto orderDto) {
        Double totalPrice =
                holding.getAveragePrice() * holding.getNumberOfShares() + orderDto.getFilledPrice() * orderDto
                        .getNumberOfShares();
        return totalPrice / getTotalShares(holding, orderDto);
    }

    private Long getTotalShares(Holding holding, OrderDto orderDto) {
        return holding.getNumberOfShares() + orderDto.getNumberOfShares();
    }

    private User findUser(String userName) {
        return userRepository.findByUserName(userName)
                .orElseThrow(() -> new NotFoundException("User not found:" + userName));
    }

    private Optional<Account> findByUser(String userName) {
        return findByUser(findUser(userName));
    }

    private Optional<Account> findByUser(User user) {
        return accountRepository.findByUser(user);
    }

    private Double calculateAmountToRelease(OrderDto orderDto) {
        return (orderDto.getNumberOfShares() * orderDto.getMaxPrice()) - (orderDto.getNumberOfShares() * orderDto
                .getFilledPrice());
    }

    private NotFoundException accountNotFount(String userName) {
        return new NotFoundException("Account not found for user: " + userName);
    }

    private void sendEmailNotification(String userName, String email) {
        emailNotificationService.sendEmail(constructEmail(userName, email));
    }

    private Email constructEmail(String userName, String email) {
        return Email.builder()
                .to(email)
                .subject("Account Created")
                .text("Dear " + userName + ", your PayDay account is created. Happy Tradings!!!")
                .build();
    }
}
