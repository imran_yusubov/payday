package com.payday.bank.exceptions;

/**
 * Thrown in case of the wrong transition applied
 */
public class InvalidStateException extends IllegalArgumentException {

    public InvalidStateException() {
        super();
    }

    public InvalidStateException(String s) {
        super(s);
    }

    public InvalidStateException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidStateException(Throwable cause) {
        super(cause);
    }
}
