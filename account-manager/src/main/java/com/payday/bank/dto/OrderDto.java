package com.payday.bank.dto;

import com.payday.bank.common.dto.AccountDto;
import com.payday.bank.model.orders.OrderStatus;
import java.util.Date;
import lombok.Data;

@Data
public class OrderDto {

    private Long id;
    private Double maxPrice;
    private Double filledPrice;
    private String stockId;
    private Long numberOfShares;
    private String type;
    private OrderStatus status;
    private AccountDto account;
    private Date created;
    private Date updated;
}
