package com.payday.bank.controller;

import com.payday.bank.common.dto.CreateOrderDto;
import com.payday.bank.common.dto.GenericSearchDto;
import com.payday.bank.common.dto.StockDto;
import com.payday.bank.common.model.PStock;
import com.payday.bank.dto.OrderDto;
import com.payday.bank.service.OrderService;
import com.payday.bank.service.StocksService;
import java.security.Principal;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("stocks")
public class StockController {

    private final OrderService orderService;
    private final StocksService stocksService;

    @PostMapping("/search")
    public Page<StockDto> searchAll(@RequestBody GenericSearchDto filter, Pageable pageable) {
        return stocksService.findAll(filter, pageable);
    }

    @PostMapping("/preferential")
    public Iterable<PStock> searchAllPreferential() {
        return stocksService.findAllPreferential();
    }

    @PostMapping("/orders")
    public OrderDto placeOrder(@Valid @RequestBody CreateOrderDto createOrderDto, Principal user) {
        OrderDto orderDto = orderService.placeOrder(createOrderDto, user.getName());
        return orderDto;
    }

    @PostMapping("/orders/search")
    public Page<OrderDto> searchAll(@RequestBody GenericSearchDto filter, Pageable pageable, Principal user) {
        return orderService.findAllOrders(filter, pageable, user.getName());
    }

}
