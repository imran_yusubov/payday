package com.payday.bank.controller;

import com.payday.bank.common.dto.AccountDto;
import com.payday.bank.service.AccountService;
import java.security.Principal;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("accounts")
public class AccountController {

    private final AccountService accountService;

    @GetMapping
    public AccountDto getAccount(Principal userPrincipal) {
        log.trace("Get account {}", userPrincipal.getName());
        return accountService.findAccountByUserName(userPrincipal.getName());
    }

    @PostMapping
    public void createAccount(Principal userPrincipal) {
        log.trace("Create account {}", userPrincipal.getName());
        accountService.createAccount(userPrincipal.getName());
    }

    @PostMapping("deposit")
    private AccountDto accountOperation(@NotNull @Positive @RequestParam Long amount, Principal userPrincipal) {
        log.trace("{} depositing", userPrincipal.getName());
        return accountService.deposit(amount,userPrincipal.getName());
    }
}
