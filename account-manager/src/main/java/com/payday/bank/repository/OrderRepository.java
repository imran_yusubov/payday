package com.payday.bank.repository;

import com.payday.bank.model.orders.Order;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface OrderRepository extends PagingAndSortingRepository<Order, Long>,
        JpaSpecificationExecutor<Order> {

}
