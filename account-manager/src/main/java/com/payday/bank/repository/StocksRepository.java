package com.payday.bank.repository;

import com.payday.bank.model.Stock;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface StocksRepository extends PagingAndSortingRepository<Stock, Long>,
        JpaSpecificationExecutor<Stock> {

}
