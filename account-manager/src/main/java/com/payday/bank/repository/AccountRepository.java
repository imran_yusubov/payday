package com.payday.bank.repository;

import com.payday.bank.common.model.user.User;
import com.payday.bank.common.model.Account;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;

public interface AccountRepository extends CrudRepository<Account, Long> {

    Optional<Account> findByUser(User user);
}
