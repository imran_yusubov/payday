package com.payday.bank.events;

import com.payday.bank.common.dto.AccountDto;
import com.payday.bank.service.AccountService;
import com.payday.bank.common.dto.Email;
import com.payday.bank.dto.OrderDto;
import com.payday.bank.notifications.EmailNotificationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class OrderStatusChangeEventHandler implements ApplicationListener<OrderStatusChangeEvent> {

    private final AccountService accountService;
    private final EmailNotificationService emailNotificationService;

    @Async
    @Override
    public void onApplicationEvent(OrderStatusChangeEvent event) {
        log.trace("Order status changed from {} to {}", event.getOldStatus(), event.getOrderDto().getStatus());
        sendNotificationEmail(event.getOrderDto());
    }

    private void sendNotificationEmail(OrderDto orderDto) {
        emailNotificationService.sendEmail(constructEmail(orderDto));
    }

    private Email constructEmail(OrderDto orderDto) {
        final AccountDto accountById = accountService.findAccountById(orderDto.getAccount().getId());
        accountById.getUser().getEmail();
        return Email
                .builder()
                .to(accountById.getUser().getEmail())
                .subject(orderDto.getStockId() + " order status update")
                .text("Dear " + accountById.getUser().getUserName() + " , your order to " + orderDto.getType() + " "
                        + orderDto.getStockId()
                        + " for price " + orderDto.getMaxPrice() + " is " + orderDto.getStatus())
                .build();
    }

}
