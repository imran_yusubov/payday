package com.payday.bank.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Stock.class)
public abstract class Stock_ {

	public static volatile SingularAttribute<Stock, Double> current;
	public static volatile SingularAttribute<Stock, String> stockId;
	public static volatile SingularAttribute<Stock, String> name;
	public static volatile SingularAttribute<Stock, String> index;
	public static volatile SingularAttribute<Stock, Long> id;
	public static volatile SingularAttribute<Stock, Double> close;
	public static volatile SingularAttribute<Stock, Double> open;

	public static final String CURRENT = "current";
	public static final String STOCK_ID = "stockId";
	public static final String NAME = "name";
	public static final String INDEX = "index";
	public static final String ID = "id";
	public static final String CLOSE = "close";
	public static final String OPEN = "open";

}

